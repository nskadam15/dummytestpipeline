#!/bin/bash

pushd $scripts_dir/YQTool
echo "Current Dir : "$PWD

if [[ -f ${yq_tool} ]]; then

    echo "**********Read Complete File*************"
    cat yq_data.yaml | ${yq_tool}

    echo "**********Read Company Name*************"
    cat yq_data.yaml | ${yq_tool} -e .company

    echo "**********Read Domains*************"
    cat yq_data.yaml | ${yq_tool} -e .domain

else
    echo "JQ_Tool not present..."
fi

popd
