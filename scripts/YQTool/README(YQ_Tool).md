<span style=" font-family: 'Consolas';font-size:14px">

# YQ Tool Testing

## Purpose
- Purpose of this tool is to perform read/write operations on .YML file using YQ Tool.

## 1. Prerequisite
- YQ Tool must be installed.

## 2. Constitution

### 2.1 Directory structure
|FileName | Description |
|---|---|
|yq_data.yml|Dummy data as .YML file.|
|Read_YAML.sh|Shell script to call .YML file.|
|README.md|User manual|

### 2.2 YQ Tool Module

|Module| Version|
|---|---|
|JQ Tool| v4.19.1 |


## 3. Input
|File Type|File Name|Description|Example|
|---|---|---|---|
| .YML file |yq_data.yml|YML input file to peform read/write operations on.|yq_data.yml|

## 4. Output

|File Name|Description|
|---|---|
|NA|Output will be generated on terminal

## 5. Tools

* v4.19.1

## 6. Execution Steps

### Step 1 : Executing .sh file 
- Read_YAML.sh will be executed.
- It has commands to read the .yml file.

### Step 2 : Fetching and displaying data 

- After executing Read_YAML.sh file, it will fetch data from yq_data.yml file and display the requested data on the terminal.
 


#### Use Case 1 :
- We can access and extract the data in any given .yml file. 

## 7. Test Case Criteria
|Case| Description|
|---|---|
|Pass| Output will be printed |
|Fail| YQ not installed <br/> YML not present |

## 8. Sample Output
![Semantic description of image](../Images/YQ1.PNG)
![Semantic description of image](../Images/YQ2.PNG)
