# The rules for compiler & archiver
CC ?= gcc
AR ?= ar
CFLAGS = -Wall -O2 -Wno-unknown-pragmas -DGCC

.SUFFIXES: .c .o
.c.o:
	$(CC) $(CFLAGS) $(INCLUDE) -c $< -o $@ -w

$(TARGET): $(OBJS)
	$(AR) rcs $@ $(OBJS)

# The rules for linkage
LINK_TARGET = UnitTest.exe
$(LINK_TARGET):
	$(CC) $(LDFLAGS) $(INCLUDE) $(SOURCES) $(LINK_PATH) $(LINK_OBJS) -o $(LINK_TARGET) -w



# The rules for macro dump
.PHONY: macro_dump
CFLAGS_MACRO = -Wall -dN -E -O2 -Wno-unknown-pragmas -DGCC
macro_dump:
	@SOURCES="$(SOURCES)";\
	for src in $$SOURCES; do\
		echo $$src;\
		output=$${src%.c}.macro;\
		$(CC) $(CFLAGS_MACRO) $(INCLUDE) -c $$src < /dev/null> $$output;\
	done


# The rules for cleaning
.PHONY: clean
clean:
	$(RM) $(TARGET) $(OBJS) $(SRCS) $(LINK_TARGET)

