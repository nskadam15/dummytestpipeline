CC = cctc

# The rules for compiler & archiver
CFLAGS 					= --cpu=tc39x      				\
						  --debug-info					\
                          -t                          \
                          --emit-locals=-equ,-symbols \
                          --section-info=-console     \
                          --iso=99                    \
                          --language=-gcc,-strings    \
                          --switch=auto               \
                          -O0                         \
                          --align=4                   \
                          --default-a0-size=0         \
                          --default-a1-size=0         \
                          --tradeoff=4                \
                          -Wc-N0                      \
                          -DT1_ENABLE

LB = artc
LBFLAGS = -cr $@

.SUFFIXES: .c .o
.c.o:
	$(CC) $(CFLAGS) $(INCLUDE) -c $< -o $@

$(TARGET): $(OBJS)
	$(LB) $(LBFLAGS) $(OBJS)

# The rules for linkage
LDFLAGS = --cpu=tc39x
LINK_TARGET = UnitTest.elf
$(LINK_TARGET):
	$(CC) $(LDFLAGS) $(INCLUDE) $(SOURCES) $(LINK_PATH) $(LINK_OBJS) -o $(LINK_TARGET)

# The rules for cleaning
MAP = $(LINK_TARGET:.elf=.map)
MDF = $(LINK_TARGET:.elf=.mdf)
.PHONY: clean
clean:
	$(RM) $(TARGET) $(OBJS) $(SRCS) $(LINK_TARGET) $(MAP) $(MDF)
