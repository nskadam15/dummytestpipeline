<span style=" font-family: 'Consolas';font-size:14px">

# Reactis Tool Testing

## Purpose
- Purpose of this tool is to create execution report using matlab model and testVectors.

## 1. Pre requisite
- matlab model, rom file, rsi and csv file must be present.
- Matlab and Reactis Tool must be installed.

## 2. Constitution

### 2.1 Directory structure
|FileName | Description |
|---|---|
|Dummy.slx|Dummy data as Matlab model.|
|Dummy.csv|Dummy test vectors to test matlab model|
|Rom.m |Dummy data Rom file|
|MatlabShellScript.sh|Shell script to call MIL_Test_ReactisNew.m file.|
|MIL_Test_ReactisNew.m| matlab and rectis executable code present|
|README.md|User manual|

### 2.2 Require Tool Module

|Module| Version|
|---|---|
|Matlab| v2016a |
|Reactis|v2021.2


## 3. Input
|File Type|File Name|Description||
|---|---|---|---|
|Dummy.slx|Dummy data as Matlab model.|
|Dummy.csv|Dummy test vectors to test matlab model|
|Dummy.rsi|Reactis file for Dummy data|
|Rom.m |Dummy data Rom file|
|MatlabShellScript.sh|Shell script to call MIL_Test_ReactisNew.m file.|
|MIL_Test_ReactisNew.m| matlab and rectis executable code present|

## 4. Output

|File Name|Description|
|---|---|
|Dummy.html|Output will be generated on gitlab artifacts folder.

## 5. Tools

* Matlab v2016a 
* Reactis v2021.2

## 6. Execution Steps

### Step 1 : Executing .sh file 
- Once pipeline trigger MatlabShellScript.sh will be executed.
- In shell scripts we are changing directory to execute reactis files.
- In shell scripts only we adding working directory path in matlab and calling MIL_Test_ReactisNew and passing variables in it.

### Step 2 : Generate execution report

- Once MIL_Test_ReactisNew called it will try to invoke matlab and reactis. Using reactis it will try to create .html execution report.
 
#### Use Case 1 : All (To Create report for all variants)
- We can create execution report using model, rom, rsi file.


## 7. Test Case Criteria
|Case| Description|
|---|---|
|Pass| html file will get generated |
|Fail| Reactis not installed <br/> Matlab not present |

## 8. Sample Output
![Semantic description of image](../Images/Reactis.png)
