%csv_file = 'ConvertStrTrqCalibResult.csv';
%model_file = 'ConvertStrTrqCalibResult.slx';
%rsi_file = 'ConvertStrTrqCalibResult.rsi';
%retCode = Model_Test_ReactisNew (model_file, csv_file, rsi_file);


function retcode = Model_Test_ReactisNew (model_file,rsi_file, csv_file )
    addpath(genpath('C:\Program Files\Reactis V2020.2\lib\api'));
	addpath(genpath('C:\Program Files\Reactis V2020.2\lib\api\MATLAB\reactis'));
    setenv('MW_MINGW64_LOC','C:\TDM-GCC-64')
	addpath('C:\Program Files\Reactis V2020.2\lib\api\MATLAB\reactis')
    try
        retcode = Reactis_simulation (model_file, rsi_file, csv_file);
    catch MExc
        retcode = 1;
        fprintf ('There was an error! The message was:\n%s\n\n', getReport (MExc, 'extended'));
        return;
    end
    
end

function retcode = Reactis_simulation (model_file, rsi_file, csv_file)
    try
        % Open Reactis Session
        fprintf ('Opening Reactis Session...\n');
        rsOpen();

		% Open Rsi file
        fprintf ('[%s]Opening RSI File...%s\n', datetime, rsi_file);
        rsiId = rsRsiOpen (rsi_file, model_file);
        % fprintf ('[%s]Setting parameters...\n', datetime);
       
        % RSI sync
        %rsRsiSave (rsiId, rsi_file);
        %fprintf('RSI Sync');
        %rsRsiSetParameterValue(rsiId,'OnIntegerOverflow','warning');
		%rsRsiSyncInputs(rsiId);
		%rsRsiSyncOutputs(rsiId);
		%rsRsiSave (rsiId, rsi_file);
        
        % Start simulator session
        fprintf ('[%s]Opening Reactis Simulator session...\n', datetime);
        simId = rsSimOpen (model_file, rsi_file);
         
        % Import test suite (.csv)
        fprintf ('[%s]Opening Test Suite...%s\n', datetime, csv_file);
        suiteId = rsSimImportSuite (simId, csv_file);
        
        % Execute simulation
        reportFilename = strrep (model_file, '.slx', '.html');
        fprintf ('[%s]Running Reactis Simulator...\n', datetime);
        rsSimRunSuite(simId, suiteId, reportFilename);
        
        fprintf ('[%s]Creating Test Report...%s\n', datetime, reportFilename);
        
        % Close simulation session
        fprintf ('Closing Reactis Simulation session...\n');
        rsSimClose (simId);
        
        % Close the test suite
        fprintf ('Closing Test Suite...\n');
        rsSuiteClose (suiteId);
        
        % Close Reactis Session
        fprintf ('[%s]Closing Reactis Session...\n', datetime);
        rsClose();
        
        fprintf ('Done...\n');
        retcode = 0;
    catch MExc
        fprintf ('There was an error! The message was:\n%s\n\n', getReport (MExc, 'extended'));
        retcode = 1;
    end
end