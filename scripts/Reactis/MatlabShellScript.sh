#!/bin/sh
scripts_dir=$1
echo " executing shell"
dir=$(pwd)
dir1="$dir/scripts/Reactis/"
cd $dir1
echo $PWD	
mkdir -p ${dir}/artifacts/Reactis   
matlab -nodesktop -nosplash -minimize -wait -log -r "retcode=1;
	try,
	    addpath(genpath('$dir1'));
        disp('entered in Reactis try...');
        target_slx='Dummy.slx';
        target_rsi='Dummy.rsi';
		target_csv='Dummy.csv';
		disp(target_csv);
        retcode = MIL_Test_ReactisNew (target_slx, target_rsi, target_csv);					
	catch e,
		retcode = 1;
		disp('Error file created');
	end;
	exit(retcode);"
cp -f Dummy.html ${dir}/artifacts/Reactis/
echo "*********************************************************"

	
