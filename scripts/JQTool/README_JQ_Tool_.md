<span style=" font-family: 'Consolas';font-size:14px">

# JQ Tool Testing

## Purpose
- Purpose of this tool is to perform read/write operations on .JSON file using JQ Tool.

## 1. Prerequisite
- JQ Tool must be installed.

## 2. Constitution

### 2.1 Directory structure
|FileName | Description |
|---|---|
|jq_data.json|Dummy data as .JSON file.|
|Read_Json.sh|Shell script to call .JSON file.|
|README.md|User manual|

### 2.2 JQ Tool Module

|Module| Version|
|---|---|
|JQ Tool| v1.6 |


## 3. Input
|File Type|File Name|Description|Example|
|---|---|---|---|
| .JSON file |jq_data.json|JSON input file to peform read/write operations on.|jq_data.json|

## 4. Output

|File Name|Description|
|---|---|
|NA|Output will be generated on terminal

## 5. Tools

* JQ v1.6

## 6. Execution Steps

### Step 1 : Executing .sh file 
- Read_Json.sh will be executed.
- It has commands to read the .json file.

### Step 2 : Fetching and displaying data 

- After executing Read_Json.sh file, it will fetch data from jq_data.json file and display the requested data on the terminal.
 


#### Use Case 1 : All (To Create report for all variants)
- We can access and extract the data in any given .json file. 

## 7. Test Case Criteria
|Case| Description|
|---|---|
|Pass| Output will be printed |
|Fail| JQ not installed <br/> JSON not present |

## 8. Sample Output
![Semantic description of image](../Images/JQ1.PNG)
![Semantic description of image](../Images/JQ2.PNG)
