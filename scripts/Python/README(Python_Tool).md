<span style=" font-family: 'Consolas';font-size:14px">

# Python Tool Testing

## Purpose
- Purpose of this tool is to create .txt file from excel using .py script.

## 1. Prerequisite
- Python Tool must be installed.

## 2. Constitution

### 2.1 Directory structure
|FileName | Description |
|---|---|
|test.py|Dummy data as .PY file.|
|Book1.xlsx|Dummy data present inside this excel|
|main.sh|Shell script to call .py file.|
|README.md|User manual|

### 2.2 Python Tool Module

|Module| Version|
|---|---|
|Python Tool| v3.7.0 |
|openpyxl|v3.1.1


## 3. Input
|File Type|File Name|Description||
|---|---|---|---|
| test.py file |test.py|.py will execute using Book1.xlsx |

## 4. Output

|File Name|Description|
|---|---|
|Book1.txt|Output will be generated on gitlab artifacts folder.

## 5. Tools

* Python v3.7
* openpyxl v3.1.1

## 6. Execution Steps

### Step 1 : Executing .sh file 
- Once pipeline trigger main.sh will be executed.
- From that file test.py will get invoke.

### Step 2 : Excel file creation

- once test.py file invoked using openpyxl library, from Book1.xlsx Book1.txt will get generated.
 
#### Use Case 1 : All (To Create report for all variants)
- We can convert Excel file to text file using this scripts.


## 7. Test Case Criteria
|Case| Description|
|---|---|
|Pass| Output will be printed |
|Fail| Python not installed <br/> Excel not present |

## 8. Sample Output
![Semantic description of image](../Images/Python.png)
