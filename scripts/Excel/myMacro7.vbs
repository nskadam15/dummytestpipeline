Dim objFso, objExcel, FilePath, scriptdir
scriptdir = CreateObject("Scripting.FileSystemObject").GetParentFolderName(WScript.ScriptFullName)
FilePath = scriptdir + "/Test.csv"
Set objFso = CreateObject("Scripting.FileSystemObject")
Set objExcel = CreateObject("Excel.Application")

objExcel.Workbooks.Open(FilePath)
objExcel.Worksheets(1).Cells(1,1) = "Hello Excel"
objExcel.ActiveWorkbook.Save

objExcel.Quit
Set objExcel = Nothing
