#!/bin/bash

ROOT_DIR=${PWD}
echo "ROOT_DIR : "${ROOT_DIR}

pushd $scripts_dir/Excel
echo "Current Dir : "$PWD

if [[ -f $PWD/Test.xlsx ]]; then
    echo "Removing Existing xlsx File"
    rm $PWD/Test.csv
fi

echo "Creating New Excel File"
#ouch $PWD/Test.xlsx
cscript myMacro2.vbs

echo "Writing data in Excel file"
cscript myMacro7.vbs

mkdir -p ${ROOT_DIR}/artifacts/Excel
cp -f $PWD/Test.csv ${ROOT_DIR}/artifacts/Excel

popd
