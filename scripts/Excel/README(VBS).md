<span style=" font-family: 'Consolas';font-size:14px">

# Excel Tool Testing

## Purpose
- Purpose of this tool is to perform create/read/write operations on Excel/CSV file using VBS Tool.

## 1. Prerequisite
- Excel Tool must be installed.

## 2. Directory structure
|FileName | Description |
|---|---|
|myMacro2.vbs|vbs script to create csv file.|
|myMacro7.vbs| vbs script to write data in csv file|
|vbs.sh|Shell script to call .vbs file.|
|README(VBS).md|User manual|

## 3. Input
|File Name|Description|
|---|---|
|NA|-|

## 4. Output

|File Type|File Name|Description|
|---|---|---|
|.csv file |Tcs.csv|New File with data written.|

## 5. Tools

|Module| Version|
|---|---|
|Excel| 2016 |

## 6. Execution Steps

### Step 1 : Executing .sh file 
- vbs.sh will be executed.
- It has commands to call myMacro2.vbs script which will create new Test.csv file.
- After file creation vbs.sh script will call myMacro7.vbs to write data in Test.csv file.

### Step 2 : Fetching and displaying data 

- After executing vbs.sh file, it will create new Test.csv file with data written. And file will get uploaded on artifact
 
#### Use Case 1 :
- We can create and read/write the data in any .csv/.xlsx file. 

## 7. Test Case Criteria
|Case| Description|
|---|---|
|Pass| file will get created with data written in it |
|Fail| Excel not installed |

## 8. Sample Output
![Excel Output](../Images/Excel.PNG)
