<span style=" font-family: 'Consolas';font-size:14px">

# Strawberry Perl Tool Testing

## Purpose
- Purpose of this tool is to verify perl tool using .pl and .sh file

## 1. Prerequisite
- Strawberry Perl Tool must be installed.

## 2. Constitution

### 2.1 Directory structure

#### 2.1.1 scripts Directory structure
|FileName | Description |
|---|---|
|StrawberryPerl/perltest.pl |Dummy data as .pl file.|
|StrawberryPerl/perlshell1.sh | Shell script file. 

### 2.2 Strawberry Perl Tool Module

|Module| Version|
|---|---|
|Strawberry Perl| v5.28.0.1-64bit |


## 3. Input
|File Type|File Name|Description|Example|
|---|---|---|---|
| .pl file |perltest.pl|.pl input file to perform print operations.|perltest.pl|

## 4. Output

|File Name|Description|
|---|---|
|NA|Output will be generated on terminal

## 5. Tools

* Strawberry Perl 5.28.0.1-64bit

## 6. Execution Steps

### Step 1 : Executing .sh file 
- perlshell1.sh will be executed.
- It has commands to read the .pl file.

### Step 2 : Fetching and displaying data 

- After executing perlshell1.sh file, it will fetch data from perltest.pl file and display the requested data on the terminal.
 


#### Use Case 1 : All (To Create report for all variants)
- We are reading .pl file data. 

## 7. Test Case Criteria
|Case| Description|
|---|---|
|Pass| Output will be printed |
|Fail| Strawberry Perl not installed <br/> .pl not present |

## 8. Sample Output
![Semantic description of image](../Images/Strawberry_Perl.png)
