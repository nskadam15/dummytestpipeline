#!/bin/bash

pushd $scripts_dir/Jfrog
echo "Current Dir : "$PWD

#Jfrog user Job_details
jfrog_username=${JFROG_USERNAME}
jfrog_password=${JFROG_PASSWORD}

#private access token for gitlabee
private_token=${PRIVATE_ACCESS_TOKEN}

#Jfrog url
JFROG_Artifactory_URL="https://spaws.jp.nissan.biz/artifactory/"
echo "JFROG_Artifactory_URL : ${JFROG_Artifactory_URL}"

    - ${jfrog_cli} --version

popd
