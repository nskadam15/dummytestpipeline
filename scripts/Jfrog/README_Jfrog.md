<span style=" font-family: 'Consolas';font-size:14px">

# Jfrog Tool Testing

## Purpose
- Purpose of this tool is to Download/Upload file on artifact.

## 1. Prerequisite
- Jfrog Tool must be installed.

## 2. Constitution

### 2.1 Directory structure
|FileName | Description |
|---|---|
|Jfrog.sh|Shell script which will display the version pf Jfrog.|
|README.md|User manual|

### 2.2 Jfrog Tool Module

|Module| Version|
|---|---|
|Jfrog Tool| v2 |

## 3. Input
|File Type|File Name|Description||
|---|---|---|---|
|NA || |

## 4. Output

|File Name|Description|
|---|---|
|NA||

## 5. Tools

* Jfrog v2

## 6. Execution Steps

### Step 1 : Executing .sh file 
- Once pipeline trigger Jfrog.sh will be executed and will display version of Jfrog.

## 7. Test Case Criteria
|Case| Description|
|---|---|
|Pass| Jfrog version will be printed |
|Fail| Jfrog not installed |
