<span style=" font-family: 'Consolas';font-size:14px">

# Matlab Tool Testing

## Purpose
- Purpose of this tool is to compile matlab model.

## 1. Pre requisite
- matlab model, rom file must be present
- Matlab Tool must be installed.

## 2. Constitution

### 2.1 Directory structure
|FileName | Description |
|---|---|
|Test.slx|Test data as Matlab model.|
|RomName.m |Dummy data Rom file|
|ModelCompileChecker.sh|Shell script to call Check_Model_Compile1.m file.|
|Check_Model_Compile1.m| matlab executable code present|
|README.md|User manual|

### 2.2 Require Tool Module

|Module| Version|
|---|---|
|Matlab| v2016a |


## 3. Input
|File Type|File Name|Description||
|---|---|---|---|
|Test.slx|Test data as Matlab model.|
|RomName.m |Dummy data Rom file|
|ModelCompileChecker.sh|Shell script to call Check_Model_Compile1.m file.|
|Check_Model_Compile1.m| matlab executable code present|

## 4. Output

|File Name|Description|
|---|---|
|NA|Output will be generated on gitlab terminal.

## 5. Tools

* Matlab v2016a 

## 6. Execution Steps

### Step 1 : Executing .sh file 
- Once pipeline trigger ModelCompileChecker.sh will be executed.
- In shell scripts we are changing directory to execute .m files.
- In shell scripts only we adding working directory path in matlab and calling Check_Model_Compile1 and passing variables in it.

### Step 2 : Generate execution report

- Once Check_Model_Compile1 called it will try to invoke matlab . Using reactis it will try to compile matlab model.
 
#### Use Case 1 : All (To Create report for all variants)
- We can compile matlab model.


## 7. Test Case Criteria
|Case| Description|
|---|---|
|Pass| Output can we see on terminal |
|Fail| Matlab not present |

## 8. Sample Output
![Semantic description of image](../Images/Matlab.png)
