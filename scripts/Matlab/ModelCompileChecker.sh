scripts_dir=$1
echo " executing shell"
dir=$(pwd)
dir1="$dir/scripts/Matlab/"
echo "$dir1"
cd $dir1
echo $PWD
matlab -nodesktop -nosplash -minimize -wait -log -r "retCode=1;
      try,
          addpath(genpath('$dir1'));
          model_name = 'Test';
          rom_name = 'RomName';
          disp('entering into matlab')
          version
	    retCode = Check_Model_Compile1(model_name, rom_name);
      catch e,
		retCode = 1;
		fprintf('There was an error! The message was:\n%s\n\n', getReport(e,'extended'));
      end;
	exit(retCode);"
