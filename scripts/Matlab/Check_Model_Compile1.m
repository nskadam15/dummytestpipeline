%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2018 Developed by Nissan Motor Company, Limited
% Use of software is subject to a specific license granted by Nissan Motor Company, Limited
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%retValue = check_Model_Compile(model_name, rom_name)
function retvalue = Check_Model_Compile1(model_name, rom_name)
    retvalue = 1;
    model_name = 'Test';
    rom_name = 'RomName';
    fprintf('*********************************\n');
    fprintf('Model Compile script\n');
    fprintf('*********************************\n');
    fprintf('Model : %s\n\n',model_name);
    try
        disp('Matlab Try loaded...')
        % load rom.m file
        evalin('base',[rom_name ';']);
        % need to load before compile
        load_system(model_name);
        open_system(model_name);
        % compile the model
        evalin('base',[model_name '([],[],[],''compile'')']);
        fprintf('Model Compiled Scuccessfully!!!\n');
    catch MExc
        retvalue = 1;
        evalin('base',[model_name '([],[],[],''term'')']);
        fprintf('There was an error! The message was:\n%s\n\n', getReport(MExc,'extended'));
        return
    end
    evalin('base',[model_name '([],[],[],''term'')']);
    retvalue = 0;
end


