#!/bin/bash

function print_ACmdErrorLog () {
    
	ACmdErrorLog=($(find ${ROOT_DIR} -type f -name 'A*ErrorLog.txt'))
	if [[ "${ACmdErrorLog}" != "" ]];then
		let "x=${#ACmdErrorLog[@]} - 1"
		for i in $(seq 0 ${x});do
			if [[ $(grep "." ${ACmdErrorLog[$i]}) != "" ]];then
				echo -e "\nWinAMS ErrorLog File Path	:	${ACmdErrorLog[$i]}\n\n"
				echo -e "\n#####################################################################\n\n"
				grep "." ${ACmdErrorLog[$i]}
				echo -e "\n#####################################################################\n\n"
				break
			fi
		done
	fi
}

function winams_execute () {
    PROJECT_FILE="$1"
    NEW_OBJ_FILE="$2"
    NEW_XLO_FILE="$3"
    OUTPUT_DIR="$4"
    COVERAGE_OPT="$5"
    RESET_FLG="$6"
    
    if [ $RESET_FLG = "1" ]; then
        RUN_OPT="-runC"
    else
        RUN_OPT="-runS"
    fi

    echo "PROJECT_FILE:" ${PROJECT_FILE}
    echo "NEW_OBJ_FILE:" ${NEW_OBJ_FILE}
    echo "NEW_XLO_FILE:" ${NEW_XLO_FILE}
    echo "OUTPUT_DIR:" ${OUTPUT_DIR}
    echo "COVERAGE_OPT:" ${COVERAGE_OPT}
    echo "RUN_OPT:"${RUN_OPT}

    Project_dir=$(dirname "${PROJECT_FILE}");
    XLO_REL_PATH=$(realpath --relative-to="$Project_dir/" "${NEW_XLO_FILE}")
    OBJ_REL_PATH=$(realpath --relative-to="$Project_dir/" "${NEW_OBJ_FILE}")
    OUT_REL_PATH=$(realpath --relative-to="$Project_dir/" "${OUTPUT_DIR}")

    sed -i "s|^ObjectFile.*|ObjectFile=${XLO_REL_PATH}|g" ${PROJECT_FILE}
    sleep 2
	sed -i "s|^InFile.*|InFile=${OBJ_REL_PATH}|g" ${PROJECT_FILE}
    sleep 2
	sed -i "s|^OutDir.*|OutDir=${OUT_REL_PATH}|g" ${PROJECT_FILE}
    sleep 2
    
    echo "Start Executing WinAMS Simulation..."
    /C/winAMS/BIN/AmsCommand.exe -startAMS "${PROJECT_FILE}"
    print_ACmdErrorLog
    #AMSCommand -obj "${NEW_OBJ_FILE}" "${PROJECT_FILE}"
    #AMSCommand -set_omf "${NEW_XLO_FILE}" "${PROJECT_FILE}"
    #AMSCommand -output "${OUT_REL_PATH}" "${PROJECT_FILE}"
    /C/winAMS/BIN/AmsCommand.exe -b "${RUN_OPT}" "${COVERAGE_OPT}" "${PROJECT_FILE}"
    print_ACmdErrorLog
    /C/winAMS/BIN/AmsCommand.exe -endAMS
    print_ACmdErrorLog
    echo "End Executing WinAMS Simulation..."
}

function elf_xlo_convert ()
{
    echo "Start Converting Elf to XLO..."
    SRC_FILE=$1;
    DST_FILE=$2;
    OMF_XLO_FILE=$(basename ${SRC_FILE}).xlo
    /C/winAMS/BIN/TriCoreomf.exe $SRC_FILE
    mv $OMF_XLO_FILE $DST_FILE
    
    echo "End Converting Elf to XLO..."
}

function generate_select_csv () {
    TARGET_CATALOG_CSV="$1"
    OUTPUT_FILE="$2"
    
    echo "[TEST]" > ${OUTPUT_FILE}
    echo "TestTitle=" >> ${OUTPUT_FILE}
    echo "ActTestCsvNo=1" >> ${OUTPUT_FILE}
    echo "ActTestCsvName0="${TARGET_CATALOG_CSV} >> ${OUTPUT_FILE}
    echo "encode=0" >> ${OUTPUT_FILE}
}
