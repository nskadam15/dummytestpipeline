<span style=" font-family: 'Consolas';font-size:14px">

# GCC Tool Testing

## Purpose
- Purpose of this tool is to create ".a" file using ".c" and ".h"

## 1. Prerequisite
- GCC Tool must be installed.

## 2. Constitution

### 2.1 Directory structure

#### 2.1.1 Workspace Directory structure
|FileName | Description |
|---|---|
|Dummy/sample.c|Dummy data as .c file.|
|UnitTest/UnitTest.c | Dummy data as .c file.|
|Dummy/include/sample.h|Header file present as .h file|
|makefile.mk | makefile for compilation at workspace level |
|UnitTest/makefile.mk | makefile for compilation at workspace/UnitTest level | 

#### 2.1.1 scripts Directory structure
|FileName | Description |
|---|---|
|Winams/Compilation.sh | shell script | 
|common_files/Makefile_common.mk | Makefile will call from shell script|
|Winams/README.md|User manual|

### 2.2 GCC Tool Module

|Module| Version|
|---|---|
|GCC| v9.3.x |


## 3. Input
|File Type|File Name|Description|
|---|---|---|
|C file|Dummy/sample.c|Dummy data as .c file.|
|C file|UnitTest/UnitTest.c | Dummy data as .c file.|
|h file|Dummy/include/sample.h|Header file present as .h file|

## 4. Output

|File Name|Description|
|---|---|
|libSample.a|Output file will get generated at artifacts/GCC

## 5. Tools

* GCC v9.3.x

## 6. Execution Steps

### Step 1 : Executing .sh file 
- shell script will call from yml script.
- Makefile_common.mk default having GCC data. 

### Step 2 : Executing makefile

- "make clean" and "make" command will get executed at workspce level and workspace/UnitTest level.
 


#### Use Case 1 : All (To Create report for all variants)
- We can can create .a file using .c and .h files. 

## 7. Test Case Criteria
|Case| Description|
|---|---|
|Pass| .a file will get created |
|Fail| GCC not present |

## 8. Sample Output
![Semantic description of image](../Images/GCC.PNG)
