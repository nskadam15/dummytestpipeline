#!/bin/bash

swcName=$1
scripts_dir=$2
SIL_script_folder=$3

ROOT_DIR=${PWD}
echo "ROOT_DIR : "${ROOT_DIR}

WORKSPACE_DIR=${ROOT_DIR}/workspace/
UNIT_TEST_DIR=${WORKSPACE_DIR}/UnitTest

Model_Test_Winams_Script_Name="WinAMS_testing.sh"

echo -e "###############################################\n"
echo -e "Environment Creation Start_time: $(date +"%T")\n"
echo -e "###############################################\n"

echo -e "\n--------------------------------------------------------------------"
echo -e "-------WinAMS Environment Creation Automation Framework Started-------"
echo -e "----------------------------------------------------------------------\n"

envrionment_log=${scripts_dir}/${SIL_script_folder}/ENV_Log.txt

source ${scripts_dir}/${SIL_script_folder}/${Model_Test_Winams_Script_Name}

WORKSPACE_DIR=${ROOT_DIR}/workspace/${swcName}
UNIT_TEST_DIR=${WORKSPACE_DIR}/UnitTest
CODE_DIR=${WORKSPACE_DIR}/..

echo -e "WORKSPACE_DIR : "$WORKSPACE_DIR
echo -e "CODE_DIR : "$CODE_DIR

ELF_L=${ROOT_DIR}/artifacts/ELF_Generation/*.elf
if [ ! -d ${ROOT_DIR}/artifacts/Winams/ELF/1.ELF ]; then
    mkdir -p ${ROOT_DIR}/artifacts/Winams/ELF/1.ELF
    cp -R ${ELF_L} ${ROOT_DIR}/artifacts/Winams/ELF/1.ELF/
fi
cp -R ${ELF_L} ${ROOT_DIR}/test_data/1.Environment/
XLO_L=${ROOT_DIR}/artifacts/Winams/ELF/1.ELF/UnitTest.elf.xlo
echo -e "Please wait...OMF conversion for 1st elf is in progress...\n\n"
elf_xlo_convert ${ELF_L} ${XLO_L}
if [[ -f ${XLO_L} ]]; then

    echo -e "\n1st elf has been converted to xlo successfully...\n\n"

    cp -R ${XLO_L} ${ROOT_DIR}/test_data/1.Environment/
    cp -f ${scripts_dir}/${SIL_script_folder}/winAMS/sample_switch.amsy ${scripts_dir}/${SIL_script_folder}/
    mv ${scripts_dir}/${SIL_script_folder}/winAMS/sample_switch.amsy ${scripts_dir}/${SIL_script_folder}/${swcName}.amsy
    
    pushd ${ROOT_DIR}/test_data/1.Environment
    echo -e "Please wait...Linking "${swcName}".amsy with SrcPathList...\n\n"
    sed -i 's+CURRENTPATH+'${PWD}'+g'  ${scripts_dir}/${SIL_script_folder}/${swcName}.amsy
    popd
    sed -i 's/SECONDXLO/.\\UnitTest.elf.xlo/g' ${scripts_dir}/${SIL_script_folder}/${swcName}.amsy
    sed -i 's/SECONDELF/.\\UnitTest.elf/g' ${scripts_dir}/${SIL_script_folder}/${swcName}.amsy
    rm ${ROOT_DIR}/test_data/1.Environment/*.amsy
    cp -f ${scripts_dir}/${SIL_script_folder}/${swcName}.amsy ${ROOT_DIR}/test_data/1.Environment/ 
    if [ ! -d ${ROOT_DIR}/artifacts/Winams/WinAMS/ ]; then
        mkdir ${ROOT_DIR}/artifacts/Winams/WinAMS/
    fi
    cp -f ${scripts_dir}/${SIL_script_folder}/${swcName}.amsy ${ROOT_DIR}/artifacts/Winams/WinAMS/

    ARTIFACT_DIR=${ROOT_DIR}/artifacts/Winams
    if [[ ! -d "$ARTIFACT_DIR" ]]; then
        mkdir ${ARTIFACT_DIR}
    fi
    echo "ARTIFACT_DIR : " ${ARTIFACT_DIR}

    echo "Executing winAMS Test..."
    RESET_FLG="1"
    TOTAL_CSV=0
    REPORT_GE_OK=0
    REPORT_GE_NG=0
    let TOTAL_CSV++

    TESTCASE_DIR=$(cygpath -u $(readlink -f "${ROOT_DIR}/test_data/2.TestCase/"));      # Ex.) full path for "nissan_b1_doc/SWE/SWE.5_SoftwareIntegrationAndIntegrationTest/3.B2B Test/YAW/2.Test Case"
    TESTENV_DIR=$(cygpath -u $(readlink -f "${ROOT_DIR}/test_data/1.Environment/"));    # Ex.) full path for "nissan_b1_doc/SWE/SWE.5_SoftwareIntegrationAndIntegrationTest/3.B2B Test/YAW/1.Envrionment"
    echo "TESTCASE_DIR :" ${TESTCASE_DIR}
    echo "TESTENV_DIR  :" ${TESTENV_DIR}
    
    ELF_DIR=${ARTIFACT_DIR}/ELF/1.ELF
    echo "ELF DIR :" ${ELF_DIR}
    ELF_FILE=$(basename "UnitTest" ".elf")
    echo "ELF FILE :" $ELF_FILE
    ELF_1=${ELF_DIR}/${ELF_FILE}.elf
    XLO_1=${ELF_DIR}/${ELF_FILE}.elf.xlo

    echo "ELF Path :" ${ELF_1}
    echo "XLO Path :" ${XLO_1}

    WINAMS_PROJ_FILE=$(find ${TESTENV_DIR}/*.amsy)
    echo "WINAMS_PROJ_FILE : "${WINAMS_PROJ_FILE}
    UNIT_TEST_ELF_FULL_PATH=$(cygpath -u $(readlink -f ${ELF_1}))
    echo "UNIT_TEST_ELF_FULL_PATH : "${UNIT_TEST_ELF_FULL_PATH}
    UNIT_TEST_XLO_FULL_PATH=$(cygpath -u $(readlink -f ${XLO_1}))
    echo "UNIT_TEST_XLO_FULL_PATH : "${UNIT_TEST_XLO_FULL_PATH}
    WINAMS_PROJ_FILE_FULL_PATH=$(cygpath -u $(readlink -f "${WINAMS_PROJ_FILE}"))
    echo "WINAMS_PROJ_FILE_FULL_PATH : "${WINAMS_PROJ_FILE_FULL_PATH}
    OUTPUT_DIR_FULL_PATH=$(cygpath -u $(readlink -f ${ARTIFACT_DIR}/Report))
    echo "OUTPUT_DIR_FULL_PATH : "${OUTPUT_DIR_FULL_PATH}
    cat ${TESTCASE_DIR}/select_csv.dat
    # execute WinAMS Simulation
    winams_execute "${WINAMS_PROJ_FILE_FULL_PATH}" "${UNIT_TEST_ELF_FULL_PATH}" "${UNIT_TEST_XLO_FULL_PATH}" "${OUTPUT_DIR_FULL_PATH}" -mcdc ${RESET_FLG}
            
    #Checking if report is generated or nissan_tool
    if [[ -f "${OUTPUT_DIR_FULL_PATH}/TestReport.htm" ]]; then
        let REPORT_GE_OK++
    else
        let REPORT_GE_NG++
    fi

    #judgement part
    if [[ ${REPORT_GE_OK} = 1 ]]; then
        echo "Passed"
        exit 0
    elif [[ ${REPORT_GE_OK} -eq 0 ]]; then
        echo "Failed"
        exit 1
    fi

else
    echo -e "\n1st elf has not been converted to xlo successfully...\n\n"
    exit 1
fi
