<span style=" font-family: 'Consolas';font-size:14px">

# WINAMS Tool Testing

## Purpose
- Purpose of this tool is to create ".xlo" file using ".elf" and test ".csv" test vector on winams project.

## 1. Prerequisite
- Tasking and WinAMS Tool must be installed.

## 2. Constitution

### 2.1 Directory structure

#### 2.1.1 test_data Directory structure
|FileName | Description |
|---|---|
|TestCase/add.csv|Test data as .csv file.|
|TestCase/subtract.csv | Test data as .csv file.|
|Environment|This folder contain all require dependent files for Winams tool|

#### 2.1.1 scripts Directory structure
|FileName | Description |
|---|---|
|Winams/Winams_Automation.sh | shell script | 
|Winams/winAMS/sample_switch.amsy | Winams project file|
|Winams/README.md|User manual|

### 2.2 Winams Tool Module

|Module| Version|
|---|---|
|WinAMS| R8.0 |
|Tasking| v6.2r2|


## 3. Input
|File Type|File Name|Description|
|---|---|---|
|CSV file|TestCase/add.csv|Test data as add.csv file.|
|CSV file|TestCase/subtract.csv | Test data as subtract.csv file.|
|ELF file|artifacts/ELF_Generation/UnitTest.elf| unitTest.elf as input file|

## 4. Output

|File Name|Description|
|---|---|
|.xlo|Output file will get generated at artifacts/ Winams/ ELF/ 1.ELF
|Reports|Reports directory contains all reports generated using winams tool|
|winams| winams directory contains Dummy.amsy project file|

## 5. Tools

* WinAMS R8.0 
* Tasking v6.2r2

## 6. Execution Steps

### Step 1 : Conversion of .elf to .xlo
- shell script will call from yml script.
- Here we are trying to create .xlo file using .elf. so, firstly it will check .elf generated on artifacts folder
- If .elf file present then and then .xlo conversion will start.


### Step 2 : Creating winams project

-  Once .xlo conversion successful it create winams project using sample_switch.amsy, 1st elf and 1st xlo 

### Step 3 : Executing test vector

- After successful creation of winams project test executing start and reports generated in artifacts folder


## 7. Test Case Criteria
|Case| Description|
|---|---|
|Pass| reports will get generated |
|Fail| Case1: ELF file not present. <br> Case2: NG test report.|

## 8. Sample Output
![Semantic description of image](../../Images/winams.PNG)
