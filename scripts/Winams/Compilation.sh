#!/bin/bash

SWCNAME=$1
COMPILER=$2

ROOT_DIR=${PWD}
echo "ROOT_DIR : "${ROOT_DIR}

#--- directry definition ---
WORKSPACE_DIR=${ROOT_DIR}/workspace/
UNIT_TEST_DIR=${WORKSPACE_DIR}/UnitTest

ARTIFACT=GCC

if [ $COMPILER == "Tasking" ]; then
	# change compiler back to gcc
	echo "Changing compiler back to gcc..."
	cp ${scripts_dir}/common_files/Makefile_common_tasking.mk ${scripts_dir}/common_files/Makefile_common.mk
	ARTIFACT=ELF_Generation
fi

pushd ${WORKSPACE_DIR} > /dev/null
make clean
make
popd > /dev/null

pushd ${UNIT_TEST_DIR} > /dev/null
make clean
make
popd > /dev/null 

# change compiler back to gcc
echo "Changing compiler back to gcc..."
cp ${scripts_dir}/common_files/Makefile_common_gcc.mk ${scripts_dir}/common_files/Makefile_common.mk

# check for compile error
echo "Check for compile error..."
COMPILE_ERROR=false
if [ $COMPILER == "Tasking" ]; then
	# stub function ELF files
	# '-1' is required to exclude current directory
	echo "Checking the object file existence..."
	if [ ! -e ${UNIT_TEST_DIR}/*.elf ]; then
		echo "UnitTest.elf is not exist inside UnitTest folder..."
		echo "Error in generating ELF for SWC..."
		COMPILE_ERROR=true
	fi
else
	# archive file
	echo "Checking the object file existence..."
	if [ ! -e ${WORKSPACE_DIR}/*.a ]; then
		echo "Error in Generating Archive File for SWC"
		COMPILE_ERROR=true
	fi
fi

# result
if [ $COMPILE_ERROR == false ]; then
	echo "Compilation has successed!"
	echo "Check results are in ${ROOT_DIR}/artifacts/$ARTIFACT/"
	mkdir -p ${ROOT_DIR}/artifacts/$ARTIFACT;
	if [ -e ${UNIT_TEST_DIR}/UnitTest.elf ]; then mv -f ${UNIT_TEST_DIR}/UnitTest.elf ${ROOT_DIR}/artifacts/$ARTIFACT/; fi
	if [ -e ${UNIT_TEST_DIR}/UnitTest.map ]; then mv -f ${UNIT_TEST_DIR}/UnitTest.map ${ROOT_DIR}/artifacts/$ARTIFACT/; fi
	if [ -e ${WORKSPACE_DIR}/*.a ]; then mv -f ${WORKSPACE_DIR}/*.a ${ROOT_DIR}/artifacts/$ARTIFACT/; fi
	cd ${ROOT_DIR}
	exit 0
else
	echo "Failed Compilation!"
	cd ${ROOT_DIR}
	exit 1
fi
