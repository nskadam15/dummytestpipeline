<span style=" font-family: 'Consolas';font-size:14px">

# Tasking Tool Testing

## Purpose
- Purpose of this tool is to create ".elf" file using ".c" and ".h"

## 1. Prerequisite
- Tasking Tool must be installed.

## 2. Constitution

### 2.1 Directory structure

#### 2.1.1 Workspace Directory structure
|FileName | Description |
|---|---|
|Dummy/sample.c|Dummy data as .c file.|
|UnitTest/UnitTest.c | Dummy data as .c file.|
|Dummy/include/sample.h|Header file present as .h file|
|makefile.mk | makefile for compilation at workspace level |
|UnitTest/makefile.mk | makefile for compilation at workspace/UnitTest level | 

#### 2.1.1 scripts Directory structure
|FileName | Description |
|---|---|
|Winams/Compilation.sh | shell script | 
|common_files/Makefile_common.mk | Makefile will call from shell script|
|Winams/README.md|User manual|

### 2.2 Tasking Tool Module

|Module| Version|
|---|---|
|Tasking| v6.2r2 |


## 3. Input
|File Type|File Name|Description|
|---|---|---|
|C file|Dummy/sample.c|Dummy data as .c file.|
|C file|UnitTest/UnitTest.c | Dummy data as .c file.|
|h file|Dummy/include/sample.h|Header file present as .h file|

## 4. Output

|File Name|Description|
|---|---|
|UnitTest.elf|Output file will get generated at artifacts/ELF_Generation

## 5. Tools

* Tasking v6.2r2 

## 6. Execution Steps

### Step 1 : Executing .sh file 
- shell script will call from yml script.
- Makefile_common.mk default having Makefile_common_gcc.mk file data. so, we will copy Makefile_common_tasking.mk data in Makefile_common.mk 

### Step 2 : Executing makefile

- "make clean" and "make" command will get executed at workspce level and workspace/UnitTest level.
- once above commands run we need to revert back Makefile_common.mk to Makefile_common_gcc.mk
 


#### Use Case 1 :
- We can can create .elf file using .c and .h files. 

## 7. Test Case Criteria
|Case| Description|
|---|---|
|Pass| .elf file will get created |
|Fail| Tasking not present |

## 8. Sample Output
![Semantic description of image](../Images/Tasking.PNG)
