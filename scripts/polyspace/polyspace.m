function retValue = polyspace_fun(target_1, target_2, include_1, job_type, result_dir)

    sources = horzcat(target_1, target_2);
    sources = strjoin(sources,',');

    includes = horzcat(include_1);
    includes = strjoin(includes,',');

    if (job_type == "RedCheck")
        matlab_product = ver;
        for i=1:length(matlab_product)
            if (strfind(matlab_product(i).Name,'Polyspace Code Prover Server'))
                Polyspace='polyspaceServer';
                break;
            elseif (strfind(matlab_product(i).Name,'Polyspace Code Prover'))
                Polyspace='Codeprover';
                break;
            end
        end

        if (strcmp(Polyspace,'Codeprover'))
            Polyspace_API.Polyspace_Codeprover_Funct(sources,includes,result_dir)
        else
            Polyspace_API.Polyspace__CodeproverServer_Funct(sources,includes,result_dir)
        end
    else
        matlab_product = ver;
        for i=1:length(matlab_product)
            if(strfind(matlab_product(i).Name,'Polyspace Bug Finder Server'))
                disp('Calling Polyspace BugFinderServer Function...')
                Polyspace_API.Polyspace__BugFinderServer_Funct(sources,includes,result_dir)
            elseif (strfind(matlab_product(i).Name,'Polyspace Bug Finder'))
                disp('Calling Polyspace BugFinder...')
                Polyspace_API.Polyspace_BugFinder_Funct(sources,includes,result_dir)            
            end
        end
    end
