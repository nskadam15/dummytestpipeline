#!/bin/bash

swcName=$1
job_type=$2
scripts_dir=$3
ROOT_DIR=${PWD}
echo "ROOT_DIR : "${ROOT_DIR}
target_1=${ROOT_DIR}/workspace/Dummy/sample.c
target_2=${ROOT_DIR}/workspace/UnitTest/UnitTest.c
include_1=${ROOT_DIR}/workspace/Dummy

pushd ${scripts_dir}
TOOLS_DIR=$(cd $(dirname $0); pwd -W);
popd

artifact_path=${ROOT_DIR}/artifacts/${job_type}

if [ -d $artifact_path ];then
    mkdir -p $artifact_path
fi

matlab -nodesktop -nosplash -minimize -wait -log -r "retCode=1;
				try,
                    addpath(genpath('$TOOLS_DIR'));
                    retCode = polyspace_fun($target_1,$target_2,$include_1,$job_type,$artifact_path)
                catch e,
					retCode = 1;
					fprintf('There was an error! The message was:\n%s\n\n', getReport(e,'extended'));
				end;
				exit(retCode);" 

if [ $? -eq 0 ]; then
	echo "polyspace check has successed!"
	exit 0
else
	echo "polyspace check has failed!"
	exit 1
fi
