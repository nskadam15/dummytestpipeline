start log/all
on error then continue
set unit/all
assign 0xf0000000#0x10000000
assign 0x00000000#0x10000000
@reset
;set mode source


assign /read/write/exec 0x0:0xffffffff
assign /read/exec 0x80000000:0x80000019
assign /read/exec 0x80002024:0x8000202b
assign /read/exec 0x8000203c:0x80002445
assign /read/exec 0xa0000020:0xa0000023
assign /read/exec 0xa00fc000:0xa00fc009
assign /read/exec 0xa00fc020:0xa00fc029
assign /read/exec 0xa00fc040:0xa00fc049
assign /read/exec 0xa00fc060:0xa00fc069
assign /read/exec 0xa00fc080:0xa00fc089
assign /read/exec 0xa00fc0a0:0xa00fc0a9
assign /read/exec 0xa00fc0c0:0xa00fc0c9
assign /read/exec 0xa00fc0e0:0xa00fc0e9
assign /read/exec 0xa00fc100:0xa00fc109
assign /read/exec 0xa00fc120:0xa00fc129
assign /read/exec 0xa00fc140:0xa00fc149
assign /read/exec 0xa00fc160:0xa00fc169
assign /read/exec 0xa00fc180:0xa00fc189
assign /read/exec 0xa00fc1a0:0xa00fc1a9
assign /read/exec 0xa00fc1c0:0xa00fc1c9
assign /read/exec 0xa00fc1e0:0xa00fc1e9
assign /read/exec 0xa00fc200:0xa00fc209
assign /read/exec 0xa00fc220:0xa00fc229
assign /read/exec 0xa00fc240:0xa00fc249
assign /read/exec 0xa00fc260:0xa00fc269
assign /read/exec 0xa00fc280:0xa00fc289
assign /read/exec 0xa00fc2a0:0xa00fc2a9
assign /read/exec 0xa00fc2c0:0xa00fc2c9
assign /read/exec 0xa00fc2e0:0xa00fc2e9
assign 0110004H:0ffffffH
;Starting up winAMS
CREATE PROCESS/USER/INTERFACE=THREAD winAMS.DLL
SET PROCESS/CONTROL winAMS
	wait/process/timeout=3600000000 "WinAMS"=6
	exit
